﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;


namespace UnauthenticatedPagesFinder
{

    class Program
    {
        

        static void DisplayUsage()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine(" UnauthenticatedPagesFinder -a (report all)");
            Console.WriteLine(" UnauthenticatedPagesFinder -f (only failed)");
            Console.WriteLine(" UnauthenticatedPagesFinder -fe (only failed and exclude static content)");
            Console.WriteLine(" UnauthenticatedPagesFinder -s https://url.com/ (single)");
        }

        static void Main(string[] args)
        {


            System.Console.WriteLine("1.0.1");


            bool displayAll = false;
            bool excludeStatic = false;

            PageResponseChecker checker = new PageResponseChecker();

            string[] URLs = File.ReadAllLines("URLs.txt");


            if (args.Length == 1)
            {
                string parameter = args[0];

                if (parameter == "-a")
                {
                    displayAll = true;
                }
                else if (parameter == "-f")
                {
                    displayAll = false;
                }
                else if (parameter == "-fe")
                {
                    displayAll = false;
                    excludeStatic = true;

                }
                else
                {
                    DisplayUsage();
                    return;
                }

                Logger.Clear();

                foreach (string URL in URLs)
                {
                    checker.ReportURLContainsString(LinkToURL(URL), "password", displayAll, false, excludeStatic);
                }


            }
            else if (args.Length == 2)
            {

                Logger.Clear();

                string parameter = args[0];
                string URL = args[1];

                checker.ReportURLContainsString(LinkToURL(URL), "password", displayAll, true, excludeStatic);

            }
            else
            {
                DisplayUsage();
                return;
            }

        }


        static string LinkToURL(string link)
        {
            link = link.Replace("<a href='", "");
            link = link.Replace("'>link</a>","");

            return link;
        }
    }
}

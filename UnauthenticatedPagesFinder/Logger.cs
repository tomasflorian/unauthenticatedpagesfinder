﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UnauthenticatedPagesFinder
{
    class Logger
    {
        public static void Log(string text)
        {
            Console.Write(text);
            StreamWriter stream = File.AppendText("log.txt");
            stream.Write(text);
            stream.Close();
        }

        public static void Clear()
        {
            File.Delete("log.txt");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace UnauthenticatedPagesFinder
{
    class PageResponseChecker
    {
        public enum Response
        {
            OK_Pass,
            Notfound_Pass,
            Notfound_Fail,
            OK_Fail
        }

        public Response AssertURLResponseContainsString(string URL, string responseString, bool debug)
        {
            try
            {
                // Create a request for the URL. 
                WebRequest request = WebRequest.Create(URL);
                // If required by the server, set the credentials.
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.
                WebResponse response = request.GetResponse();
                // Display the status.
                string status = ((HttpWebResponse)response).StatusDescription;

                // Get the stream containing content returned by the server.
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();


                if (debug)
                {
                    Logger.Log(responseFromServer);
                }
                // Display the content.
                //Console.WriteLine(responseFromServer);
                // Clean up the streams and the response.
                reader.Close();
                response.Close();

                if (status == "OK")
                {

                    if (responseFromServer.ToLower().Contains(responseString.ToLower()))
                    {

                        return Response.OK_Pass;


                    }
                    else
                    {
                        return Response.OK_Fail;
                    }
                }
                else
                {
                    return Response.Notfound_Pass;
                }
            }
            catch (Exception e)
            {
                return Response.Notfound_Pass;
            }
        }


        public void ReportURLContainsString(string URL, string responseString, bool displayAll, bool debug, bool excludeStatic)
        {

            
            
            if(excludeStatic && (URL.ToLower().EndsWith(".jpg") || URL.ToLower().EndsWith(".gif") || URL.ToLower().EndsWith(".css") || URL.ToLower().EndsWith(".png") || URL.ToLower().EndsWith(".js")))
            {
                return;
            }

            Response r = AssertURLResponseContainsString(URL, responseString, debug);

            if (r == Response.OK_Pass)
            {
                if (displayAll)
                {
                    Logger.Log(" " + URL + " ");
                    Logger.Log("OK\r\n");
                }
                
            }
            else if (r == Response.Notfound_Pass)
            {
                if (displayAll)
                {
                    Logger.Log(URL + " ");
                    Logger.Log("OK - page not found\r\n");
                }
                
            }
            else if (r == Response.OK_Fail)
            {
                Logger.Log(URL + " ");
                Logger.Log("FAIL - unauthenticated access\r\n");

                
            }
            else
            {
                Logger.Log(URL + " ");
                Logger.Log("Unknown error\r\n");
                
            }



            
        }

        private string Trim(string input)
        {
            int trimSize = 50;

            if (input.Length > trimSize)
            {
                int extraChars = input.Length - trimSize;


                string trimmedString = "..." + input.Substring(extraChars);

                return trimmedString;

            }
            else
            {

                return input;
            }

        }
    }
}
